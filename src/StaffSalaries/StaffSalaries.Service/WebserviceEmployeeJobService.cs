﻿using StaffSalaries.Service.DataContracts;
using EJ = EmployeeJobWebServiceProxyTypesNamespace;

namespace StaffSalaries.Service
{
    public class WebserviceEmployeeJobService : IEmployeeJobService
    {
        private readonly EJ.EmployeeJobWebService _webServiceProxy;

        public WebserviceEmployeeJobService(string webServiceUrl)
        {
            _webServiceProxy = new EJ.EmployeeJobWebService {Url = webServiceUrl};
        }

        public JobListResponse GetJobList()
        {
            return _webServiceProxy.GetJobList().ConvertToJobListResponse();
        }

        public EmployeeListResponse GetEmployeeList(EmployeeListRequest employeeListRequest)
        {
            var employeeListRequestProxy = employeeListRequest.ConvertToEmployeeListRequestProxy();

            return _webServiceProxy.GetEmployeeList(employeeListRequestProxy).ConvertToEmployeeListResponse();
        }

        public EmployeeUpdateSalaryResponse EmployeeUpdateSalary(
            EmployeeUpdateSalaryRequest employeeUpdateSalaryRequest)
        {
            var employeeUpdateSalaryRequestProxy =
                employeeUpdateSalaryRequest.ConvertToEmployeeUpdateSalaryRequestProxy();

            return _webServiceProxy.EmployeeUpdateSalary(employeeUpdateSalaryRequestProxy)
                .ConvertToEmployeeUpdateSalaryResponse();
        }
    }
}
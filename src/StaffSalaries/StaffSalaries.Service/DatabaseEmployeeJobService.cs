﻿using System.Linq;
using StaffSalaries.Model.Employees;
using StaffSalaries.Model.Jobs;
using StaffSalaries.Service.DataContracts;

namespace StaffSalaries.Service
{
    public class DatabaseEmployeeJobService : IEmployeeJobService
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IJobRepository _jobRepository;

        public DatabaseEmployeeJobService(IJobRepository jobRepository, IEmployeeRepository employeeRepository)
        {
            _jobRepository = jobRepository;
            _employeeRepository = employeeRepository;
        }

        public JobListResponse GetJobList()
        {
            var jobListResponse = new JobListResponse
            {
                Jobs = _jobRepository.FindAll().ToArray()
            };

            return jobListResponse;
        }

        public EmployeeListResponse GetEmployeeList(EmployeeListRequest employeeListRequest)
        {
            var employeeListResponse = new EmployeeListResponse
            {
                Employees = _employeeRepository.FindBy(employeeListRequest.EmployeeListQuery).ToArray(),
                TotalNumberOfEmployeesWithSpecifiedJob =
                    _employeeRepository.GetTotalNumberWith(employeeListRequest.EmployeeListQuery.JobId)
            };

            return employeeListResponse;
        }

        public EmployeeUpdateSalaryResponse EmployeeUpdateSalary(
            EmployeeUpdateSalaryRequest employeeUpdateSalaryRequest)
        {
            var employeeUpdateSalaryResponse = new EmployeeUpdateSalaryResponse();

            var employee = _employeeRepository.FindBy(employeeUpdateSalaryRequest.EmployeeId);
            employee.Salary = employeeUpdateSalaryRequest.Salary;
            _employeeRepository.Update(employee);

            return employeeUpdateSalaryResponse;
        }
    }
}
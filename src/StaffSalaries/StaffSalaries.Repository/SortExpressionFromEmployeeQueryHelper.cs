﻿using System;
using StaffSalaries.Model.Employees;

namespace StaffSalaries.Repository
{
    public static class SortExpressionFromEmployeeQueryHelper
    {
        public static string GetSortExpressionFrom(EmployeeQuery employeeQuery)
        {
            var expression = string.Empty;

            if (employeeQuery.SortBy == EmployeesSortBy.None ||
                employeeQuery.OrderBy == EmployeesOrderBy.None)
                return expression;

            switch (employeeQuery.SortBy)
            {
                case EmployeesSortBy.FullName:
                    expression += "FullName";
                    break;
                case EmployeesSortBy.Salary:
                    expression += "Salary";
                    break;
                case EmployeesSortBy.None:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            switch (employeeQuery.OrderBy)
            {
                case EmployeesOrderBy.Ascending:
                    expression += "Ascending";
                    break;
                case EmployeesOrderBy.Descending:
                    expression += "Descending";
                    break;
                case EmployeesOrderBy.None:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return expression;
        }
    }
}
﻿using StaffSalaries.Facade;
using StaffSalaries.Facade.Models;

namespace StaffSalaries.Presentation
{
    public class EmployeeListPresenter : IEmployeeListPresenter
    {
        private readonly IEmployeeJobServiceFacade _employeeJobServiceFacade;
        private readonly IEmployeeListView _employeeListView;

        public EmployeeListPresenter(IEmployeeListView employeeListView,
            IEmployeeJobServiceFacade employeeJobServiceFacade)
        {
            _employeeListView = employeeListView;
            _employeeJobServiceFacade = employeeJobServiceFacade;
        }

        public void DisplayJobList()
        {
            var jobListPresentation = _employeeJobServiceFacade.GetJobList();

            _employeeListView.JobList = jobListPresentation.Jobs;
        }

        public void DisplayEmployeeList()
        {
            var employeeListModel = new EmployeeListModel
            {
                JobId = _employeeListView.JobId,
                SortBy = _employeeListView.SortBy,
                OrderBy = _employeeListView.OrderBy,
                PageSize = _employeeListView.PageSize,
                PageIndex = _employeeListView.PageIndex
            };

            var employeeListPresentation = _employeeJobServiceFacade.GetEmployeeList(employeeListModel);

            _employeeListView.DisplayEmployeeList(employeeListPresentation.Employees,
                employeeListPresentation.TotalNumberOfEmployeesWithSpecifiedJob);
        }

        public void UpdateEmployeeSalary()
        {
            var employeeUpdateSalaryModel = new EmployeeUpdateSalaryModel
            {
                EmployeeId = _employeeListView.EmployeeId,
                Salary = _employeeListView.Salary
            };

            _employeeJobServiceFacade.EmployeeUpdateSalary(employeeUpdateSalaryModel);
        }
    }
}
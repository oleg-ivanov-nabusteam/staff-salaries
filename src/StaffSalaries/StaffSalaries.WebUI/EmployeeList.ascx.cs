﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using StaffSalaries.Facade;
using StaffSalaries.Facade.ViewModels;
using StaffSalaries.Model.Employees;
using StaffSalaries.Presentation;
using StaffSalaries.WebUI.EmployeeListControl;

namespace StaffSalaries.WebUI
{
    public partial class EmployeeList : UserControl, IEmployeeListView
    {
        private const string ConfigKey = "Config";
        private const string OrderByKey = "OrderBy";
        private const string PageIndexKey = "PageIndex";
        private const string SortByKey = "SortBy";
        private const string TotalNumberOfRowsKey = "TotalNumberOfRows";

        private IEmployeeListPresenter _presenter;

        [Category("Config")]
        [Browsable(true)]
        [UrlProperty("*.xml")]
        public string XmlConfigFile { get; set; }

        public int JobId => int.Parse(ddlJobList.SelectedValue);
        public EmployeesSortBy SortBy => (EmployeesSortBy) ViewState[SortByKey];
        public EmployeesOrderBy OrderBy => (EmployeesOrderBy) ViewState[OrderByKey];

        public int PageSize
        {
            get
            {
                var config = (Configuration) ViewState[ConfigKey];
                return config.PageSize;
            }
        }

        public int PageIndex => (int) ViewState[PageIndexKey];

        public int EmployeeId
        {
            get
            {
                var hfEmployeeId =
                    (HiddenField) gvEmployeeList.Rows[gvEmployeeList.EditIndex].FindControl("hfEmployeeId");
                return int.Parse(hfEmployeeId.Value);
            }
        }

        public decimal Salary
        {
            get
            {
                var tbSalary = (TextBox) gvEmployeeList.Rows[gvEmployeeList.EditIndex].FindControl("tbSalary");
                return decimal.Parse(tbSalary.Text);
            }
        }

        public IEnumerable<JobViewModel> JobList
        {
            set
            {
                ddlJobList.DataSource = value;
                ddlJobList.DataBind();
            }
        }

        public void DisplayEmployeeList(IEnumerable<EmployeeViewModel> employeeList,
            int totalNumberOfEmployeesWithSpecifiedJob)
        {
            gvEmployeeList.DataSource = employeeList;
            gvEmployeeList.DataBind();

            ViewState[TotalNumberOfRowsKey] = totalNumberOfEmployeesWithSpecifiedJob;

            CreatePagingControl();
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            var config = (Configuration) ViewState[ConfigKey];
            if (config == null)
            {
                config = ConfigurationFactory.GetConfiguration(XmlConfigFile);
                ViewState[ConfigKey] = config;
            }

            _presenter = new EmployeeListPresenter(this,
                EmployeeListControlDataSourceResolverFactory.GetConfiguredIoCContainer(config)
                    .GetInstance<IEmployeeJobServiceFacade>());

            ddlJobList.DataBound += DdlJobList_DataBound;
            ddlJobList.SelectedIndexChanged += DdlJobList_SelectedIndexChanged;

            gvEmployeeList.RowCommand += GvEmployeeList_RowCommand;
            gvEmployeeList.RowEditing += GvEmployeeList_RowEditing;
            gvEmployeeList.RowCancelingEdit += GvEmployeeList_RowCancelingEdit;
            gvEmployeeList.RowUpdating += GvEmployeeList_RowUpdating;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var config = (Configuration) ViewState[ConfigKey];

                gvEmployeeList.Columns[2].Visible = config.IsEditable;

                ViewState[SortByKey] = EmployeesSortBy.None;
                ViewState[OrderByKey] = EmployeesOrderBy.None;
                ViewState[PageIndexKey] = 0;

                _presenter.DisplayJobList();
            }

            CreatePagingControl();
        }

        protected void DdlJobList_DataBound(object sender, EventArgs e)
        {
            _presenter.DisplayEmployeeList();
        }

        protected void DdlJobList_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropEditMode();
            DropPagination();

            _presenter.DisplayEmployeeList();
        }

        protected void GvEmployeeList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "SortAndOrderByFullName" ||
                e.CommandName == "SortAndOrderBySalary")
            {
                switch (e.CommandName)
                {
                    case "SortAndOrderByFullName":
                    {
                        SortAndOrderBy(EmployeesSortBy.FullName);
                        break;
                    }
                    case "SortAndOrderBySalary":
                    {
                        SortAndOrderBy(EmployeesSortBy.Salary);
                        break;
                    }
                }

                DropEditMode();
                DropPagination();

                _presenter.DisplayEmployeeList();
            }
        }

        protected void GvEmployeeList_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvEmployeeList.EditIndex = e.NewEditIndex;

            _presenter.DisplayEmployeeList();
        }

        protected void GvEmployeeList_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            DropEditMode();

            _presenter.DisplayEmployeeList();
        }

        protected void GvEmployeeList_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            _presenter.UpdateEmployeeSalary();

            DropEditMode();

            _presenter.DisplayEmployeeList();
        }

        private void DropPagination()
        {
            ViewState[PageIndexKey] = 0;
        }

        private void DropEditMode()
        {
            gvEmployeeList.EditIndex = -1;
        }

        private void SortAndOrderBy(EmployeesSortBy newSortBy)
        {
            var sortBy = (EmployeesSortBy) ViewState[SortByKey];

            ViewState[SortByKey] = newSortBy;

            ChangeColumnOrdering(sortBy != newSortBy);
        }

        private void ChangeColumnOrdering(bool hasChangedColumn)
        {
            var orderBy = (EmployeesOrderBy) ViewState[OrderByKey];

            if (hasChangedColumn)
            {
                ViewState[OrderByKey] = EmployeesOrderBy.Ascending;
                return;
            }

            switch (orderBy)
            {
                case EmployeesOrderBy.None:
                {
                    ViewState[OrderByKey] = EmployeesOrderBy.Ascending;
                    break;
                }
                case EmployeesOrderBy.Ascending:
                {
                    ViewState[OrderByKey] = EmployeesOrderBy.Descending;
                    break;
                }
                case EmployeesOrderBy.Descending:
                {
                    ViewState[OrderByKey] = EmployeesOrderBy.Ascending;
                    break;
                }
            }
        }

        private void CreatePagingControl()
        {
            var totalNumberOfRows = (int) ViewState[TotalNumberOfRowsKey];
            var pageIndex = (int) ViewState[PageIndexKey];

            pPagination.Controls.Clear();

            if (PageSize > 0 && totalNumberOfRows > PageSize)
            {
                var numberOfPages = totalNumberOfRows / PageSize;
                if (totalNumberOfRows % PageSize > 0)
                    numberOfPages++;

                var lPagesLabel = new Literal
                {
                    Text = "Pages:&nbsp;"
                };

                pPagination.Controls.Add(lPagesLabel);

                for (var i = 0; i < numberOfPages; i++)
                {
                    var lbPage = new LinkButton
                    {
                        ID = "lbPage_" + i,
                        Text = (i + 1).ToString(),
                        CommandArgument = i.ToString()
                    };
                    lbPage.Click += LbPage_Click;

                    pPagination.Controls.Add(lbPage);

                    var lSpace = new Literal {Text = "&nbsp;"};

                    pPagination.Controls.Add(lSpace);

                    if (i == pageIndex)
                    {
                        lbPage.ForeColor = ColorTranslator.FromHtml("#000000");
                        lbPage.Font.Underline = false;
                    }
                }
            }
        }

        protected void LbPage_Click(object sender, EventArgs e)
        {
            var lbPage = (LinkButton) sender;
            ViewState[PageIndexKey] = int.Parse(lbPage.CommandArgument);

            DropEditMode();

            _presenter.DisplayEmployeeList();
        }
    }
}
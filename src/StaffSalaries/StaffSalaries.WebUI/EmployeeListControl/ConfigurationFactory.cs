﻿using System.IO;
using System.Web;
using System.Xml.Serialization;

namespace StaffSalaries.WebUI.EmployeeListControl
{
    public static class ConfigurationFactory
    {
        public static Configuration GetConfiguration(string xmlConfigFile)
        {
            var absolutePhysycalFilePath = HttpContext.Current.Server.MapPath(xmlConfigFile);

            using (TextReader textReader = new StreamReader(absolutePhysycalFilePath))
            {
                var deserializer = new XmlSerializer(typeof(Configuration));
                return (Configuration) deserializer.Deserialize(textReader);
            }
        }
    }
}
﻿namespace StaffSalaries.Facade.Presentations
{
    public abstract class PresentationBase
    {
        public string Message { get; set; }
    }
}
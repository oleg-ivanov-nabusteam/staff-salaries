﻿using System.Collections.Generic;
using StaffSalaries.Facade.ViewModels;
using StaffSalaries.Model.Employees;

namespace StaffSalaries.Facade.Mappers
{
    public static class EmployeeMapperExtensionMethods
    {
        public static IEnumerable<EmployeeViewModel> ConvertToEmployeeListViewModel(
            this IEnumerable<Employee> employees)
        {
            var employeeViewModels = new List<EmployeeViewModel>();

            foreach (var employee in employees) employeeViewModels.Add(employee.ConvertToEmployeeViewModel());

            return employeeViewModels;
        }

        public static EmployeeViewModel ConvertToEmployeeViewModel(this Employee employee)
        {
            return new EmployeeViewModel
            {
                Id = employee.Id.ToString(),
                FullName = $"{employee.LastName} {employee.FirstName}",
                Salary = $"{employee.Salary:0.00}"
            };
        }
    }
}